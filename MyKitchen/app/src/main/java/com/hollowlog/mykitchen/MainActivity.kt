package com.hollowlog.mykitchen

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.hollowlog.mykitchen.grocery_list.GroceryList

class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
    }

    private lateinit var groceryListCard: CardView
    private lateinit var kitchenItemsCard: CardView
    private lateinit var recipeListCard: CardView
    private lateinit var myKitchensCard: CardView

    private lateinit var userProfileButton: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.homepage)

        // Hide the action bar at the top of the screen
        supportActionBar?.hide()

        // Initialize the user profile button and set on click listener
        userProfileButton = findViewById(R.id.user_profile_button)
        userProfileButton.setOnClickListener { launchUserProfileActivity() }

        // Initialize the CardViews to act as buttons
        groceryListCard = findViewById(R.id.grocery_list_card)
        kitchenItemsCard = findViewById(R.id.kitchen_items_card)
        recipeListCard = findViewById(R.id.recipe_list_card)
        myKitchensCard = findViewById(R.id.my_kitchens_card)

        // Set on click listeners for the CardViews
        groceryListCard.setOnClickListener { launchGroceryListActivity() }
        kitchenItemsCard.setOnClickListener { launchKitchenItemsActivity() }
        recipeListCard.setOnClickListener { launchRecipeListActivity() }
        myKitchensCard.setOnClickListener { launchMyKitchensActivity() }
    }

    /**
     * Launches an explicit intent to open up the User Profile Activity
     */
    private fun launchUserProfileActivity() {

    }

    /**
     * Launches an explicit intent to open up the Grocery List Activity
     */
    private fun launchGroceryListActivity() {
        val intent = Intent(this@MainActivity, GroceryList::class.java)

        startActivity(intent)
    }

    /**
     * Launches an explicit intent to open up the Kitchen Items Activity
     */
    private fun launchKitchenItemsActivity() {

    }

    /**
     * Launches an explicit intent to open up the Recipe List Activity
     */
    private fun launchRecipeListActivity() {

    }

    /**
     * Launches an explicit intent to open up the My Kitchens Activity
     */
    private fun launchMyKitchensActivity() {

    }
}