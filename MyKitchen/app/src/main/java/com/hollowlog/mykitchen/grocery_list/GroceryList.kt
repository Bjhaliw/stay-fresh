package com.hollowlog.mykitchen.grocery_list

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.hollowlog.mykitchen.R

class GroceryList : AppCompatActivity() {

    // Global variables
    private lateinit var groceryListRecyclerView: RecyclerView
    private lateinit var viewAdapter: GroceryListViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.grocery_list)

        groceryListRecyclerView = findViewById(R.id.grocery_list_recycler_view)
        viewAdapter = GroceryListViewAdapter(R.layout.list_item_options, this, listOf())


    }

    /**
     * Creates the menu at the top of the screen for the activity
     */
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        // Inflates the options menu, adding our custom menu to it
        menuInflater.inflate(R.menu.add_menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Handles interaction with the options menu at the top of the screen
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection within our options menu
        return when (item.itemId) {
            R.id.add_menu_button -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}