package com.hollowlog.mykitchen.login

import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

import com.hollowlog.mykitchen.R

class RegisterUser : AppCompatActivity() {

    // Global variables for the activities
    private lateinit var nameField: TextView
    private lateinit var emailField: EditText
    private lateinit var passwordField: EditText
    private lateinit var verifyPasswordField: EditText
    private lateinit var registerButton: Button
    private lateinit var progressBar: ProgressBar

    // Firebase Authentication global variable
    private lateinit var auth: FirebaseAuth
    private lateinit var db: FirebaseFirestore


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Setting the layout for the activity
        setContentView(R.layout.register_user)

        // Initializing our global variables
        nameField = findViewById(R.id.register_full_name)
        emailField = findViewById(R.id.register_email)
        passwordField = findViewById(R.id.register_password)
        verifyPasswordField = findViewById(R.id.verify_register_password)
        registerButton = findViewById(R.id.register_screen_button)
        progressBar = findViewById(R.id.progress_bar)

        // Firebase Authentication instance
        auth = FirebaseAuth.getInstance()
        db = Firebase.firestore

        // User selected the Register button
        registerButton.setOnClickListener { registerUser() }
    }

    /**
     * Verify that the user typed in all of the text fields correctly.
     */
    private fun registerUser() {
        val name = nameField.text.toString().trim()
        val email = emailField.text.toString().trim()
        val password = passwordField.text.toString().trim()
        val verifyPassword = verifyPasswordField.text.toString().trim()

        // check if the user entered a name
        if (name.isEmpty()) {
            nameField.error = "Name is required"
            nameField.requestFocus()
            return
        }

        // check if the user entered an email
        if (email.isEmpty()) {
            emailField.error = "Email is required"
            emailField.requestFocus()
            return
        }

        // check if the user entered a valid email address
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailField.error = "Please provide valid email"
            emailField.requestFocus()
            return
        }

        // check if the user entered a password
        if (password.isEmpty()) {
            passwordField.error = "Password is required"
            passwordField.requestFocus()
            return
        }

        // Check if the password is greater than 6 characters
        if (password.length < 6) {
            passwordField.error = "Minimum length is 6 characters"
            passwordField.requestFocus()
            return
        }

        // check if the user verified their password
        if (verifyPassword.isEmpty()) {
            verifyPasswordField.error = "Please verify your password"
            verifyPasswordField.requestFocus()
            return
        }

        // check if the passwords match each other
        if (password != verifyPassword) {
            verifyPasswordField.error = "Passwords do not match"
            verifyPasswordField.requestFocus()
            return
        }

        progressBar.visibility = View.VISIBLE
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener {
            // If creating a new user was successful
            if (it.isSuccessful) {

                // User object
                val user = hashMapOf<String, String>()
                user["name"] = name
                user["email"] = email

                // Add the user to the Firestore's 'users' collection
                db.collection("users").document(FirebaseAuth.getInstance().currentUser!!.uid).
                set(user).addOnSuccessListener {
                    Toast.makeText(this, "User registered successfully", Toast.LENGTH_LONG)
                        .show()
                }.addOnFailureListener {
                    Toast.makeText(this, "Failed to add to DB", Toast.LENGTH_LONG).show()
                    Log.d("Register", it.toString())
                }
            } else {
                Toast.makeText(this, "Failed to register user", Toast.LENGTH_LONG).show()
            }

            progressBar.visibility = View.GONE
            setResult(RESULT_OK)
            finish()
        }
    }
}