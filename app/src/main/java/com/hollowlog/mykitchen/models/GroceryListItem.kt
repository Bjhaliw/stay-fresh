package com.hollowlog.mykitchen.models

import java.time.LocalDate

class GroceryListItem(var name: String, var amount: String, var selected : Boolean, var addedDate : LocalDate, var addedBy : String, var comments: String) {

    override fun toString(): String {
        return "Name: $name, Amount: $amount, Selected: $selected"
    }

}