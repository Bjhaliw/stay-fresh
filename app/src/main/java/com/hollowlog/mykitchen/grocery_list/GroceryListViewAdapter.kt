package com.hollowlog.mykitchen.grocery_list

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.method.TextKeyListener.clear
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.startActivityForResult
import com.hollowlog.mykitchen.R
import com.hollowlog.mykitchen.models.GroceryListItem
import java.io.Serializable
import java.util.Collections.addAll

internal class GroceryListViewAdapter(
    private val rowLayout: Int,
    private val context: Context,
    private var list: MutableList<GroceryListItem>
) : RecyclerView.Adapter<GroceryListViewAdapter.ViewHolder>() {

    // Create ViewHolder which holds a View to be displayed
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(rowLayout, viewGroup, false)
        return ViewHolder(v)
    }

    // Binding: The process of preparing a child view to display data corresponding to a position within the adapter.
    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        viewHolder.name.text = "${list[viewHolder.adapterPosition].name} - ${list[viewHolder.adapterPosition].amount}"
        if (list[viewHolder.adapterPosition].selected) {

            viewHolder.checkBox.isChecked = true
            viewHolder.name.apply {
                paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
        } else {
            viewHolder.checkBox.isChecked = false
            list[viewHolder.adapterPosition].selected = false
            viewHolder.name.apply {
                paintFlags = paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            }
        }

        viewHolder.options.setOnClickListener {
            deleteItem(viewHolder.name.text.toString(), viewHolder.adapterPosition)
        }

        viewHolder.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                list[viewHolder.adapterPosition].selected = true
                viewHolder.name.apply {
                    paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                }
            } else {
                list[viewHolder.adapterPosition].selected = false
                viewHolder.name.apply {
                    paintFlags = paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size

    }

    class ViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        internal val name: TextView = itemView.findViewById(R.id.text)
        internal val options: TextView = itemView.findViewById(R.id.recycler_options)
        internal val checkBox: CheckBox = itemView.findViewById(R.id.checkBox)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            // Display a Toast message indicting the selected item

            Toast.makeText(
                view.context,
                "${name.text}, Pos: $adapterPosition",

                Toast.LENGTH_SHORT
            ).show()
        }
    }

    /**
     * Delete an individual item from the grocery list
     */
    private fun deleteItem(itemName: String, pos: Int) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage("Do you want to delete ${itemName}?\n\nYou cannot reverse this action")
            .setPositiveButton("Delete",
                DialogInterface.OnClickListener { dialog, id ->

                    // Remove from the list and update the screen
                   list.removeAt(pos)

                    notifyItemRemoved(pos)
                    notifyItemRangeChanged(pos, list.size);

                })
            .setNegativeButton("Cancel",
                DialogInterface.OnClickListener { dialog, id ->
                    // User cancelled the dialog
                })

        // Create the AlertDialog object and return it
        val dialog = builder.create()
        dialog.setTitle("Delete Grocery Item")
        dialog.show()
    }

}
