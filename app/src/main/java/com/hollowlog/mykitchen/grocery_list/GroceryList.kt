package com.hollowlog.mykitchen.grocery_list

import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.hollowlog.mykitchen.R
import com.hollowlog.mykitchen.login.RegisterUser
import com.hollowlog.mykitchen.models.GroceryListItem
import java.time.LocalDate


class GroceryList : AppCompatActivity() {

    // Global variables for components on screen
    private lateinit var groceryListRecyclerView: RecyclerView
    private lateinit var viewAdapter: GroceryListViewAdapter
    private lateinit var dialog: Dialog

    // Launcher to add items to the GroceryList
    private lateinit var groceryListRequest: ActivityResultLauncher<Intent>

    private var list = mutableListOf<GroceryListItem>(
        GroceryListItem("Bread", "1 Loaf", true, LocalDate.now(), "Brenton", "Buy lots of this"),
        GroceryListItem(
            "Eggs",
            "1 Dozen",
            false,
            LocalDate.now(),
            "Jessica",
            "Make sure they aren't cracked this time"
        ),
        GroceryListItem("Milk", "1 Gallon", false, LocalDate.now(), "Jeremy", "Got milk? TM")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.grocery_list)

        // Initialize components on screen
        // Get the recycler view so we can scroll through the items
        groceryListRecyclerView = findViewById(R.id.grocery_list_recycler_view)
        // Populate the recycler view with the actual items
        viewAdapter = GroceryListViewAdapter(
            R.layout.list_item_options,
            this,
            list
        )
        dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        // Attach the RecyclerView adapter to the RecyclerView
        groceryListRecyclerView.layoutManager = LinearLayoutManager(this)
        groceryListRecyclerView.adapter = viewAdapter

        // Initialize the activity result launcher
//        groceryListRequest =
//            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
//                if (result.resultCode == RESULT_OK) {
//
//                    // Get the data from the result
//                    val data = result.data
//                    val name = data?.getStringExtra("name")!!
//                    val amount = data?.getStringExtra("amount")!!
//
//                    // Add a new GroceryList Item
//                    list.add(GroceryListItem(name, amount, false))
//
//                    // Update the RecyclerView on the screen
//                    viewAdapter.notifyItemChanged(list.size - 1)
//
//                }
//            }
    }

    /**
     * Creates the menu at the top of the screen for the activity
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        // Inflates the options menu, adding our custom menu to it
        menuInflater.inflate(R.menu.add_menu, menu)

        // Hide the add to kitchen button
        val item = menu?.findItem(R.id.add_to_kitchen_button) as MenuItem
        item.isVisible = false

        // Loop through the list to see if at least one is selected
        for (i in list) {
            if (i.selected) {
                item.isVisible = true
                break // Finish the loop, we already found one
            }
        }

        return super.onCreateOptionsMenu(menu)
    }

    /**
     * Adds the selected grocery list items to the Kitchen Inventory database
     */
    private fun handleAddToKitchen(item: MenuItem) {
        // Loop through the list to see if at least one is selected

        val itr = list.iterator()
        var addList = listOf<GroceryListItem>()

        var counter = 0
        while (itr.hasNext()) {
            val curr = itr.next()

            if (curr.selected) {
                addList = addList + curr
                itr.remove()
                viewAdapter.notifyItemRemoved(counter)
                viewAdapter.notifyItemRangeChanged(counter, list.size);
            }

            counter++
        }

        item.isVisible = false

    }

    /**
     * Handles interaction with the options menu at the top of the screen
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection within our options menu
        return when (item.itemId) {
            R.id.add_menu_button -> {

                // Create an inflater to show in a dialog on the screen
                var inflater =
                    this.layoutInflater.inflate(R.layout.add_grocery_list_item, null)

                // Initialize the components for the dialog
                val nameField = inflater.findViewById<TextView>(R.id.add_grocery_list_name)
                val amountField = inflater.findViewById<TextView>(R.id.add_grocery_list_amount)
                val commentsField = inflater.findViewById<TextView>(R.id.add_grocery_list_comment)
                val submitButton = inflater.findViewById<Button>(R.id.submit_add_grocery_list)

                // When the submit button is clicked, add the new grocery list item
                submitButton.setOnClickListener {
                    list.add(
                        GroceryListItem(
                            nameField.text.toString(),
                            amountField.text.toString(),
                            false,
                            LocalDate.now(),
                            "Test User",
                            commentsField.text.toString()
                        )
                    )

                    // Update the RecyclerView on the screen
                    viewAdapter.notifyItemInserted(list.size - 1)

                    // Hide the dialog from the screen
                    dialog.hide()
                }

                // Load the dialog and then show it on the screen
                dialog.setContentView(inflater)
                dialog.show()

                true
            }

            R.id.add_to_kitchen_button -> {
                handleAddToKitchen(item)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}